#pragma once

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QKeyEvent>
#include <QTimer>
#include <QGraphicsTextItem>
#include "MapDrawer.h"
#include "Score.h"
#include "Player.h"

#include <iostream>

class Pacman : public Player
{
	Q_OBJECT
private:
	const int upgradeDuration = 100;
	const int coinsNumber = 150;

	int coinsCollected;
	int animaction;
	int animSpeed;
	int pacmanNextDirection;
	long int enemyTimer;
	bool isNextIsCellWall;
	int upgradeCounter;
	bool upgradeCollected;
	int acctiveEnemies;

	void detectColision();
	void detectUpgrades(int x, int y);
	void animatePacman();
	void moveEnemies();
	void detectPortal();
	void movePacman();
	void detectPacmanTurn();
	void updateMap();
	void checkCoins();

public:
	Pacman();
	void keyPressEvent(QKeyEvent *event);
public slots:
	void move();
};
