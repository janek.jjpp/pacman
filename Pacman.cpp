#include "Pacman.h"
#include "Game.h"

extern Game *game;

Pacman::Pacman()
{
	setPixmap(QPixmap("./images/pacmanRight.png"));
	QTimer *timer = new QTimer();
	connect(timer, SIGNAL(timeout()), this, SLOT(move()));
	timer->start(30);
	playerDirection = 0;
	pacmanNextDirection = 0;
	moveCounter = 0;
	playerPos[0] = 8;
	playerPos[1] = 16;
	isNextIsCellWall = false;
	animaction = 0;
	animSpeed = 1;
	enemyTimer = 0;
	acctiveEnemies = 1;
	upgradeCollected = false;
	upgradeCounter = 0;
	coinsCollected = 0;
}

void Pacman::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Left)
		pacmanNextDirection = 3;
	else if (event->key() == Qt::Key_Right)
		pacmanNextDirection = 4;
	else if (event->key() == Qt::Key_Up)
		pacmanNextDirection = 1;
	else if (event->key() == Qt::Key_Down)
		pacmanNextDirection = 2;
	else
		std::cout << "Dont click this, use arrow keys" << std::endl;
}

void Pacman::move()
{
	moveEnemies();
	movePacman();
	updateMap();
	detectPacmanTurn();
	animatePacman();
}

void Pacman::checkCoins()
{
	if (coinsCollected == coinsNumber)
	{
		game->message = new QGraphicsTextItem();
		game->message->setPlainText(QString("YOU WON !!!"));
		game->message->setDefaultTextColor(Qt::green);
		game->message->setFont(QFont("times", 30));
		game->message->setPos(30, 280);
		game->message->setFlag(QGraphicsItem::ItemIsFocusable);
		game->message->setFocus();
		game->scene->addItem(game->message);
		playerSpeed = 0;
		moveCounter = 0;
		animSpeed = 0;
		game->score->safeScore();
		for (int i = 0; i < 4; i++)
		{
			game->enemies[i]->playerSpeed = 0;
			game->enemies[i]->setPixmap(QPixmap("./images/emptyBlock.png"));
			game->enemies[i]->enemyEaten = true;
		}
	}
}

void Pacman::detectPacmanTurn()
{
	if (moveCounter == 0)
	{
		if (pacmanNextDirection != playerDirection)
		{
			if (!checkMap(playerPos[0], playerPos[1], pacmanNextDirection))
			{
				playerDirection = pacmanNextDirection;
				if (upgradeCollected)
				{
					if (playerDirection == 1)
						setPixmap(QPixmap("./images/pacmanUpgradedNormalUp.png"));
					else if (playerDirection == 2)
						setPixmap(QPixmap("./images/pacmanUpgradedNormalDown.png"));
					else if (playerDirection == 3)
						setPixmap(QPixmap("./images/pacmanUpgradedNormalLeft.png"));
					else if (playerDirection == 4)
						setPixmap(QPixmap("./images/pacmanUpgradedNormalRight.png"));
				}
				else
				{
					if (playerDirection == 1)
						setPixmap(QPixmap("./images/pacmanUp.png"));
					else if (playerDirection == 2)
						setPixmap(QPixmap("./images/pacmanDown.png"));
					else if (playerDirection == 3)
						setPixmap(QPixmap("./images/pacmanLeft.png"));
					else if (playerDirection == 4)
						setPixmap(QPixmap("./images/pacmanRight.png"));
				}
			}
		}
		isNextIsCellWall = checkMap(playerPos[0], playerPos[1], playerDirection);
	}
}

void Pacman::updateMap()
{
	if (moveCounter == gridSide)
	{
		updateGrid();
		detectUpgrades(playerPos[0], playerPos[1]);
	}
}

void Pacman::movePacman()
{
	if (!isNextIsCellWall)
	{
		movePlayer();
	}
}

void Pacman::detectColision()
{
	if (upgradeCollected)
	{
		for (int i = 0; i < acctiveEnemies; i++)
		{
			if (!game->enemies[i]->enemyEaten &&
				((playerPos[0] == game->enemies[i]->playerPos[0] && playerPos[1] == game->enemies[i]->playerPos[1] - 1) ||
				 (playerPos[0] == game->enemies[i]->playerPos[0] && playerPos[1] == game->enemies[i]->playerPos[1] - 2)))
			{
				//game->enemies[i]->enemyEaten = true;
				//game->enemies[i]->setPixmap(QPixmap("./images/emptyBlock.png"));
				game->enemies[i]->respawn();
				game->score->increaseScore(50);
			}
		}
	}
	else
	{
		for (int i = 0; i < acctiveEnemies; i++)
		{
			if (!game->enemies[i]->enemyEaten &&
				((playerPos[0] == game->enemies[i]->playerPos[0] && playerPos[1] == game->enemies[i]->playerPos[1] - 1) ||
				 (playerPos[0] == game->enemies[i]->playerPos[0] && playerPos[1] == game->enemies[i]->playerPos[1] - 2)))
			{
				game->message = new QGraphicsTextItem();
				game->message->setPlainText(QString("DEFEATED"));
				game->message->setDefaultTextColor(Qt::red);
				game->message->setFont(QFont("times", 33));
				game->message->setPos(50, 280);
				game->message->setFlag(QGraphicsItem::ItemIsFocusable);
				game->message->setFocus();
				game->scene->addItem(game->message);
				playerSpeed = 0;
				moveCounter = 0;
				gridSide = 999999;
				animSpeed = 0;
				game->score->safeScore();
				for (int i = 0; i < 4; i++)
					game->enemies[i]->playerSpeed = 0;
			}
		}
	}
}

void Pacman::animatePacman()
{
	animaction += animSpeed;
	if (upgradeCollected)
	{
		upgradeCounter++;
		if (animaction == 5)
		{
			if (playerDirection == 1)
				setPixmap(QPixmap("./images/pacmanUpgradedUp.png"));
			else if (playerDirection == 2)
				setPixmap(QPixmap("./images/pacmanUpgradedDown.png"));
			else if (playerDirection == 3)
				setPixmap(QPixmap("./images/pacmanUpgradedLeft.png"));
			else if (playerDirection == 4)
				setPixmap(QPixmap("./images/pacmanUpgradedRight.png"));
			animSpeed *= -1;
		}
		else if (animaction == 0)
		{
			if (playerDirection == 1)
				setPixmap(QPixmap("./images/pacmanUpgradedNormalUp.png"));
			else if (playerDirection == 2)
				setPixmap(QPixmap("./images/pacmanUpgradedNormalDown.png"));
			else if (playerDirection == 3)
				setPixmap(QPixmap("./images/pacmanUpgradedNormalLeft.png"));
			else if (playerDirection == 4)
				setPixmap(QPixmap("./images/pacmanUpgradedNormalRight.png"));
			animSpeed *= -1;
		}
		if (upgradeCounter >= upgradeDuration)
		{
			upgradeCollected = false;
			upgradeCounter = 0;
		}
	}
	else
	{
		if (animaction == 5)
		{
			if (playerDirection == 1)
				setPixmap(QPixmap("./images/pacmanEatUp.png"));
			else if (playerDirection == 2)
				setPixmap(QPixmap("./images/pacmanEatDown.png"));
			else if (playerDirection == 3)
				setPixmap(QPixmap("./images/pacmanEatLeft.png"));
			else if (playerDirection == 4)
				setPixmap(QPixmap("./images/pacmanEatRight.png"));
			animSpeed *= -1;
		}
		else if (animaction == 0)
		{
			if (playerDirection == 1)
				setPixmap(QPixmap("./images/pacmanUp.png"));
			else if (playerDirection == 2)
				setPixmap(QPixmap("./images/pacmanDown.png"));
			else if (playerDirection == 3)
				setPixmap(QPixmap("./images/pacmanLeft.png"));
			else if (playerDirection == 4)
				setPixmap(QPixmap("./images/pacmanRight.png"));
			animSpeed *= -1;
		}
	}
	detectColision();
}

void Pacman::moveEnemies()
{
	enemyTimer++;
	game->enemies[0]->move();
	if (enemyTimer > 250)
	{
		acctiveEnemies = 2;
		game->enemies[1]->move();
		if (enemyTimer > 500)
		{
			acctiveEnemies = 3;
			game->enemies[2]->move();
			if (enemyTimer > 750)
			{
				acctiveEnemies = 4;
				game->enemies[3]->move();
				enemyTimer = 750;
			}
		}
	}
}

void Pacman::detectUpgrades(int x, int y)
{
	if ((*game->map)[y][x] == 2)
	{
		coinsCollected++;
		checkCoins();
		game->score->increaseScore(5);
		(*game->map)[y][x] = 1;
		game->mapDrawer->redrawMap(x, y);
	}
	else if ((*game->map)[y][x] == 3)
	{
		coinsCollected++;
		checkCoins();
		game->score->increaseScore(15);
		(*game->map)[y][x] = 1;
		game->mapDrawer->redrawMap(x, y);
		upgradeCollected = true;
		upgradeCounter = 0;
	}
}
