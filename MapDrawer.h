#pragma once
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QImage>
#include <iostream>

class MapDrawer
{
	QGraphicsScene *scene;
	QGraphicsPixmapItem *pixMap[17][21];

public:
	MapDrawer(std::vector<std::vector<int>> *map, QGraphicsScene *scene);
	void redrawMap(int x, int y);
	void drawRect(int x, int y, int side, int col);
};
