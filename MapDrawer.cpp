#include "MapDrawer.h"

MapDrawer::MapDrawer(std::vector<std::vector<int>> *map, QGraphicsScene *scene)
{
	this->scene = scene;
	for (int iY = 0; iY < 21; iY++)
	{
		for (int iX = 0; iX < 17; iX++)
		{
			drawRect(iX, iY, 32, (*map)[iY][iX]);
		}
	}
}

void MapDrawer::redrawMap(int x, int y)
{
	delete pixMap[x][y];
}

void MapDrawer::drawRect(int x, int y, int side, int col)
{
	if (col == 0)
	{
		QGraphicsPixmapItem *cell = new QGraphicsPixmapItem();
		cell->setPixmap(QPixmap("./images/block.png"));
		cell->setPos(x * side, y * side + 32);
		scene->addItem(cell);
		pixMap[x][y] = cell;
	}
	else if (col == 1)
	{
		QGraphicsPixmapItem *cell = new QGraphicsPixmapItem();
		cell->setPixmap(QPixmap("./images/background.png"));
		cell->setPos(x * side, y * side + 32);
		scene->addItem(cell);
		pixMap[x][y] = cell;
	}
	else if (col == 2)
	{
		QGraphicsPixmapItem *coin = new QGraphicsPixmapItem();
		coin->setPixmap(QPixmap("./images/coin.png"));
		coin->setPos(x * side, y * side + 32);
		scene->addItem(coin);
		pixMap[x][y] = coin;
	}
	else if (col == 3)
	{
		QGraphicsPixmapItem *coin = new QGraphicsPixmapItem();
		coin->setPixmap(QPixmap("./images/upgrade.png"));
		coin->setPos(x * side, y * side + 32);
		scene->addItem(coin);
		pixMap[x][y] = coin;
	}
}
