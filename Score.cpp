#include "Score.h"

Score::Score(QGraphicsItem *parent) : QGraphicsTextItem(parent)
{
	score = 0;
	highScore = readHighScore();
	setPlainText(QString("High Score ") + QString::number(std::stoi(highScore)) + QString("      Score: ") + QString::number(score));
	setDefaultTextColor(Qt::white);
	setFont(QFont("times", 16));
}

void Score::increaseScore(int num)
{
	score += num;
	setPlainText(QString("High Score ") + QString::number(std::stoi(highScore)) + QString("      Score: ") + QString::number(score));
}

void Score::safeScore()
{
	if (score > std::stoi(highScore))
	{
		std::ofstream scoreFile("highScore.txt");
		scoreFile << score;
		scoreFile.close();
	}
}

std::string Score::readHighScore()
{
	std::ifstream scoreFile("highScore.txt");
	std::string hs;
	getline(scoreFile, hs);
	scoreFile.close();
	return hs;
}
