#pragma once

#include <QGraphicsTextItem>
#include <iostream>
#include <fstream>
#include <QFont>

class Score : public QGraphicsTextItem
{
public:
    Score(QGraphicsItem *parent = 0);
    void increaseScore(int num);
    void safeScore();
    std::string readHighScore();

private:
    int score;
    std::string highScore;
};
