#include "Game.h"

extern Game *game;

Game::Game(QWidget *parent) : QGraphicsView(parent)
{
	scene = new QGraphicsScene();
	scene->setSceneRect(0, 0, 544, 672);
	setScene(scene);
	this->setFixedSize(548, 736);
	this->setBackgroundBrush(QBrush(Qt::black));

	mapWidth = map[0].size();
	mapHight = map->size();
	mapDrawer = new MapDrawer(map, scene);

	score = new Score();
	score->setPos(0, -32);
	score->setFlag(QGraphicsItem::ItemIsFocusable);
	score->setFocus();
	scene->addItem(score);

	pacmanStartX = 8 * 32;
	pacmanStartY = 17 * 32;

	pacman = new Pacman();
	pacman->setPos(pacmanStartX, pacmanStartY);
	pacman->setFlag(QGraphicsItem::ItemIsFocusable);
	pacman->setFocus();
	scene->addItem(pacman);
	loadEnemies();

	show();
}

Game::~Game()
{
	delete pacman;
	for (int i = 0; i < 4; i++)
		delete enemies[i];
	delete score;
	delete mapDrawer;
	delete scene;
	delete message;
	delete game;
}

void Game::loadEnemies()
{
	enemies[0] = new EnemyRed();
	enemies[1] = new EnemyBlue();
	enemies[2] = new EnemyPurple();
	enemies[3] = new EnemyGreen();
	for (int i = 0; i < 4; i++)
	{
		scene->addItem(enemies[i]);
	}
}
