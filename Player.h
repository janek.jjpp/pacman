#pragma once

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsScene>

class Player : public QObject, public QGraphicsPixmapItem
{
public:
	int playerSpeed = 4; // must 32%speed must = 0
	int gridSide = 8;	 //  = 32 / speed
	int playerDirection; // 0- stand still, 1 - move up, 2 move down, 3 left, 4 right
	int playerPos[2];
	int moveCounter;
	void detectPortal();
	void movePlayer();
	void updateGrid();
	Player(QGraphicsItem *parent = 0);
	bool checkMap(int x, int y, int direction);
};
