#pragma once
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QImage>
#include <QWidget>
#include <QBrush>

#include "Enemies.h"
#include "MapDrawer.h"
#include "Score.h"
#include "Player.h"
#include "Pacman.h"

class Game : public QGraphicsView
{
public:
	std::vector<std::vector<int>> gameMap{
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 2, 3, 0},
		{0, 2, 0, 2, 0, 2, 0, 0, 0, 0, 0, 2, 2, 2, 0, 2, 0},
		{0, 2, 2, 2, 0, 2, 0, 2, 2, 2, 0, 2, 0, 2, 2, 2, 0},
		{0, 0, 0, 2, 0, 2, 2, 2, 0, 2, 2, 2, 0, 2, 0, 0, 0},
		{0, 2, 2, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 2, 2, 0},
		{0, 2, 0, 2, 0, 2, 2, 2, 0, 2, 2, 2, 0, 2, 0, 2, 0},
		{0, 2, 0, 2, 0, 2, 0, 0, 0, 0, 0, 2, 0, 2, 0, 2, 0},
		{0, 3, 2, 2, 0, 1, 1, 1, 1, 1, 1, 1, 0, 2, 2, 3, 0},
		{0, 0, 0, 2, 0, 1, 0, 1, 1, 1, 0, 1, 0, 2, 0, 0, 0},
		{1, 1, 1, 2, 2, 1, 0, 1, 1, 1, 0, 1, 2, 2, 1, 1, 1},
		{0, 0, 0, 2, 2, 1, 0, 0, 0, 0, 0, 1, 2, 2, 0, 0, 0},
		{0, 2, 2, 2, 0, 1, 1, 1, 1, 1, 1, 1, 0, 2, 2, 2, 0},
		{0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 0, 0, 0, 2, 0},
		{0, 2, 0, 2, 0, 2, 2, 2, 0, 2, 2, 2, 0, 2, 0, 2, 0},
		{0, 2, 2, 2, 0, 0, 0, 2, 0, 2, 0, 0, 0, 2, 2, 2, 0},
		{0, 2, 0, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 0, 2, 0},
		{0, 2, 0, 0, 2, 0, 0, 2, 0, 2, 0, 0, 2, 0, 0, 2, 0},
		{0, 2, 0, 2, 2, 2, 0, 2, 0, 2, 0, 2, 2, 2, 0, 2, 0},
		{0, 3, 2, 2, 0, 2, 2, 2, 0, 2, 2, 2, 0, 2, 2, 3, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	};
	std::vector<std::vector<int>> *map = &gameMap;

	QGraphicsScene *scene;
	MapDrawer *mapDrawer;
	Score *score;
	Pacman *pacman;
	Enemy *enemies[4];
	QGraphicsTextItem *message;

	int mapWidth;
	int mapHight;
	int RectSize = 32;
	int pacmanStartX;
	int pacmanStartY;

	Game(QWidget *parent = 0);
	~Game();
	void loadEnemies();
};
