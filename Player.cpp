#include "Player.h"
#include "Game.h"

extern Game *game;

Player::Player(QGraphicsItem *parent) : QGraphicsPixmapItem(parent)
{
	playerDirection = 0;
	moveCounter = 0;
	playerPos[0] = 8;
	playerPos[1] = 16;
}

void Player::updateGrid()
{
	if (playerDirection == 1)
		playerPos[1]--;
	else if (playerDirection == 2)
		playerPos[1]++;
	else if (playerDirection == 3)
		playerPos[0]--;
	else if (playerDirection == 4)
		playerPos[0]++;
	moveCounter = 0;
	detectPortal();
}

void Player::movePlayer()
{
	if (playerDirection == 1)
		setPos(x(), y() - playerSpeed);
	else if (playerDirection == 2)
		setPos(x(), y() + playerSpeed);
	else if (playerDirection == 3)
		setPos(x() - playerSpeed, y());
	else if (playerDirection == 4)
		setPos(x() + playerSpeed, y());
	moveCounter++;
}

void Player::detectPortal()
{
	if (x() == 0 && playerDirection == 3)
	{
		setPos(512, y());
		playerPos[0] = 16;
	}
	else if (x() == 512 && playerDirection == 4)
	{
		setPos(0, y());
		playerPos[0] = 0;
	}
}

bool Player::checkMap(int x, int y, int direction)
{
	if (direction == 1)
		y--;
	else if (direction == 2)
		y++;
	else if (direction == 3)
		x--;
	else if (direction == 4)
		x++;
	if ((*game->map)[y][x] == 0)
		return true;
	return false;
}
