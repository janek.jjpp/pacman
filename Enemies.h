#pragma once

#include <QTimer>
#include "MapDrawer.h"
#include "Score.h"
#include "Player.h"

#include <time.h>
#include <iostream>

class Enemy : public Player
{
public:
	const int enemyMoveOptimazationAmount = 2;

	int enemyMoveOptimazation;
	bool enemyEaten;

	Enemy();
	void move();
	void setRandomDireciton();
	virtual void makeMove();
	virtual void respawn();
};

class EnemyBlue : public Enemy
{
	const int movesInOneDirection = 3;
	int movesInOneDirectionCounter;

public:
	EnemyBlue();
	void respawn() override;
	void makeMove() override;
};

class EnemyGreen : public EnemyBlue
{
	const int movesInOneDirection = 2;

public:
	EnemyGreen();
	void respawn() override;
};

class EnemyPurple : public Enemy
{
	const int movesInOneDirection = 4;
	int movesInOneDirectionCounter;

public:
	EnemyPurple();
	void respawn() override;
	void makeMove() override;
	void setHalfRandomDirection();
};

class EnemyRed : public Enemy
{
public:
	EnemyRed();
	void respawn() override;
};
