#include "Enemies.h"
#include "Game.h"

extern Game *game;

Enemy::Enemy()
{
	playerDirection = 1;
	enemyMoveOptimazation = 0;
	enemyEaten = false;
	srand(time(NULL));
}

void Enemy::respawn()
{
	playerDirection = 1;
	enemyMoveOptimazation = 0;
}

void Enemy::move()
{
	if (enemyMoveOptimazation == enemyMoveOptimazationAmount)
	{
		makeMove();
		enemyMoveOptimazation = 0;
	}
	else
		enemyMoveOptimazation++;
}

void Enemy::makeMove()
{
	movePlayer();
	if (moveCounter == gridSide)
	{
		updateGrid();
		while (checkMap(playerPos[0], playerPos[1] - 1, playerDirection))
		{
			setRandomDireciton();
		}
	}
}

void Enemy::setRandomDireciton()
{
	if (playerDirection == 1)
		playerDirection += ((rand() % 2) + 2);
	else if (playerDirection == 2)
		playerDirection += ((rand() % 2) + 1);
	else if (playerDirection == 3)
		playerDirection += ((rand() % 2) - 2);
	else
		playerDirection += ((rand() % 2) - 3);
}

EnemyBlue::EnemyBlue()
{
	playerSpeed = 8;
	gridSide = 4;
	setPixmap(QPixmap("./images/enemyBlue.png"));
	respawn();
}

void EnemyBlue::respawn()
{
	playerPos[0] = 8;
	playerPos[1] = 11;
	setPos(playerPos[0] * 32, playerPos[1] * 32);
	movesInOneDirectionCounter = 0;
	playerDirection = 1;
	enemyMoveOptimazation = 0;
}

void EnemyBlue::makeMove()
{
	movePlayer();
	if (moveCounter == gridSide)
	{
		updateGrid();
		if (movesInOneDirectionCounter >= movesInOneDirection)
		{
			movesInOneDirectionCounter = 0;
			setRandomDireciton();
		}
		else
			movesInOneDirectionCounter++;
		while (checkMap(playerPos[0], playerPos[1] - 1, playerDirection))
		{
			setRandomDireciton();
			movesInOneDirectionCounter = 0;
		}
	}
}

EnemyRed::EnemyRed()
{
	playerSpeed = 8;
	gridSide = 4;
	setPixmap(QPixmap("./images/enemyRed.png"));
	respawn();
}

void EnemyRed::respawn()
{
	playerPos[0] = 8;
	playerPos[1] = 10;
	setPos(playerPos[0] * 32, playerPos[1] * 32);
	playerDirection = 1;
	enemyMoveOptimazation = 0;
}

EnemyPurple::EnemyPurple()
{
	playerSpeed = 16;
	gridSide = 2;
	setPixmap(QPixmap("./images/enemyPurple.png"));
	respawn();
}

void EnemyPurple::respawn()
{
	playerPos[0] = 7;
	playerPos[1] = 11;
	setPos(playerPos[0] * 32, playerPos[1] * 32);
	playerDirection = 1;
	enemyMoveOptimazation = 0;
	movesInOneDirectionCounter = 0;
}

void EnemyPurple::makeMove()
{
	movePlayer();
	if (moveCounter == gridSide)
	{
		updateGrid();
		if (movesInOneDirectionCounter >= movesInOneDirection)
		{
			movesInOneDirectionCounter = 0;
			setHalfRandomDirection();
		}
		else
			movesInOneDirectionCounter++;
		while (checkMap(playerPos[0], playerPos[1] - 1, playerDirection))
		{
			setRandomDireciton();
			movesInOneDirectionCounter = 0;
		}
	}
}

void EnemyPurple::setHalfRandomDirection()
{
	if (playerDirection == 1 || playerDirection == 2)
	{
		playerDirection = 2;
		playerDirection += (rand() % 3);
	}
}

EnemyGreen::EnemyGreen()
{
	playerSpeed = 16;
	gridSide = 2;
	setPixmap(QPixmap("./images/enemyGreen.png"));
	respawn();
}

void EnemyGreen::respawn()
{
	playerPos[0] = 9;
	playerPos[1] = 11;
	setPos(playerPos[0] * 32, playerPos[1] * 32);
	playerDirection = 1;
	enemyMoveOptimazation = 0;
}
